// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import './assets/css/scss/white.theme.scss'

import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue);

import Breadcrumb from './components/Common/Breadcrumb'
Vue.component('head-breadcrumb', Breadcrumb);

import Colxx from './components/Common/Colxx'
Vue.component('b-colxx', Colxx);

import VueGoodTablePlugin from 'vue-good-table';
import 'vue-good-table/dist/vue-good-table.css'

Vue.use(VueGoodTablePlugin);

Vue.config.productionTip = false;

/* eslint-disable no-new */

export default new Vue({
  el:'#app',
  router,
  render: h => h(App)
})

